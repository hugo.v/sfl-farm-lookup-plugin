let FARM_INFO_BLOCK_STATE = "HIDDEN";

const farmInfoDiv = document.createElement("div");
farmInfoDiv.className = "farmInfo bg-brown-300 py-1 px-2";

function showFarmLoading() {
    farmInfoDiv.innerHTML = `
        <div class="farmLoading">
            <p>Farm lookup plugin loading...</p>
        </div>
    `;
}

function showFarmSearching(farmId) {
    farmInfoDiv.innerHTML = `
        <div class="farmLoading">
            <p>Looking up farm ${farmId}...</p>
        </div>
    `;
}

function showFarmNoResult() {
    farmInfoDiv.innerHTML = `
        <div class="farmError">
            <p>Could not find farm</p>
            <p class="goBack">back to search</p>
        </div>
    `;
}

function showFarmError() {
    farmInfoDiv.innerHTML = `
        <div class="farmError">
            <p>Farm lookup plugin failed to load</p>
        </div>
    `;
}

function showFarmSearch() {
    async function handleSubmit(e) {
        e.preventDefault();
        const value = document.getElementById('farmInput').value;

        try {
            showFarmSearching(value);
            const response = await fetch(`https://09p340.deta.dev/sfl/expansion?f=${value}`, {
                "headers": {
                    "accept": "application/json",
                    "accept-encoding": "gzip, deflate, br",
                    "connection": "keep-alive",
                    "sec-fetch-dest": "empty",
                    "sec-fetch-mode": "cors",
                    "sec-fetch-site": "same-origin",
                    "x-requested-with": "XMLHttpRequest"
                },
            });
            const { data: farm } = await response.json();
            if (farm?.length > 0) {
                showFarmInfo(farm[0]);
                return;
            }
            showFarmNoResult();
            return false;
        } catch (error) {
            showFarmNoResult();
            return false
        }
    }

    farmInfoDiv.innerHTML = `
        <div style="width: 100%;">
            <form id="farmInfoForm" style="display: flex;">
                <input class="farmInput" id="farmInput" style="flex: .7;" name="farmInput" type="text" placeholder="Enter farm ID" />
                <button style="flex: .3;" type="submit">Lookup</button>
            </form>
        </div>
    `;

    setTimeout(() => {
        const form = document.getElementById('farmInfoForm');
        form.addEventListener("submit", handleSubmit, false);
    }, 1000);
}

function showFarmInfo(farmInfo) {
    farmInfoDiv.innerHTML = `
        <div style="width: 100%">
            <p class="formInfoTitle">Looking up farm <b>#${farmInfo.farm_id}</b></p>
            <table class="farmInfoTable">
                <thead>
                    <tr>
                        <th>Plots</th>
                        <th>Trees</th>
                        <th>Stones</th>
                        <th>Iron</th>
                        <th>Gold</th>
                        <th>Fruits</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <p>${farmInfo.plots}</p>
                            <img class="farmInfoIcon" style="transform: scale(2) translateY(-4px);" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAaCAYAAAC+aNwHAAAAAXNSR0IArs4c6QAAAjxJREFUOE/tld1LU3EYxz/POXt1M5dTtzUVhYQKQki6Ce+kqIQuuy/wDbwLghCVwAiiq4IEe6P/oIIuygUWBYE3QVT2cqFOndtcOVfb4eyc84sZWTHvvPW5eOD34/f9fR++z5uwQ5Md4tn9gF0N2NWg0kfb9sKj/l4lfkBTYHowNQcPcObm46r3VRdPh3uVNLXQHqtBXArc4DUsPi4YrOdXOHvjyX+YrUNifFCptRX0xhjNzT5uPZhFd0DEQpTOYF8XC/MmG+vLuCMxei/d2cRuusRovyqm0/iiYaIdfu5OzYLScYmF5bjxYIMoLgwcZW7OIp9bJtjSQs/IpMjMyDmVy60Rbo3hGAo7k+Hh+zRvVw1cmlCuSOFYdMb8nDwcoTYUw/BaWKsZQvURZHrsvPpWyFIbbORgx17evf7K9RfzVEgdTQfbRkTDEeHi8TY6j7TxKVWgmErTFI4jz670KTO7DA0R2huDuPYovrxc5FoiidIElF2JnuETcbqOHcB0bOY/m5Qyi9RH4781eD7ep+x8BnfDPmL7/bhMiw9vkmhiYWsKs6w41N0BP22SuSLOShZvKE7PxKRsZeHV6KDKZ1LUtUWpaw0gJQPxedhjaZQtG1tzs7RaoLCcwhWJcnps6m8W/kzm6asDqrSwRCDcjOEr4xgaNV6FTwnowo9shkC4ie6x21vEVYU0MzGkvqeTBMuw4RN8po5tK3S3oIXrOXX53vaF9O9+SIwOKVEGxYBOCAdKwobj0Dtxv4rwF1Q33wwK6cqXAAAAAElFTkSuQmCC" />
                        </td>
                        <td>
                            <p>${farmInfo.trees}</p>
                            <img class="farmInfoIcon" src="https://sunflower-land.com/game-assets/resources/wood.png" />
                        </td>
                        <td>
                            <p>${farmInfo.stones}</p>
                            <img class="farmInfoIcon" src="https://sunflower-land.com/game-assets/resources/stone.png" />
                        </td>
                        <td>
                            <p>${farmInfo.iron}</p>
                            <img class="farmInfoIcon" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKBAMAAAB/HNKOAAAABGdBTUEAALGPC/xhBQAAABJQTFRFAAAA////qLWygZeWx8/MV3J3HeDyeAAAAAF0Uk5TAEDm2GYAAAA+SURBVAjXY2BgYAgFYtZQwQAG1hAXF1cGIOEiyhDo4iJoyhAkIqgEJJWVlEMZgpWVjEMZWI2NTQOAekJDGQAHbQl34P9HEgAAAABJRU5ErkJggg==" />
                        </td>
                        <td>
                            <p>${farmInfo.gold}</p>
                            <img class="farmInfoIcon" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKBAMAAAB/HNKOAAAAGFBMVEUAAAB1HRH133DuqiDRZAXy896kPAbpz0hnuJ+VAAAAAXRSTlMAQObYZgAAAD5JREFUCNdjAAJBIGYUDBVgYBRSUlJkEFdSUgpkEC1SCnVkEA4KNQaSJsYmiQwiJsZuggyMLm6JAkA9goIMAMKHB0z3pi/7AAAAAElFTkSuQmCC" />
                        </td>
                        <td>
                            <p>${farmInfo.fruit_patches}</p>
                            <img class="farmInfoIcon" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAOCAYAAAD5YeaVAAAAAXNSR0IArs4c6QAAAOVJREFUKJGNkb1qAkEURs8VY6/gI7hWKURQsBiIlU3eIFj5GOmsfIetxDLNljYWW1hsEBEr1y6VICipBIW9Fu4sJuygp5qfw/dxZ+ABZtRTM+opgDwSa+YFgG14cctW3IYXAHbrM4U80fvoqk27p5gnv/+c+H4tsVufs7N4MpNMHntNtetVWvsZ/QLQjxeSJY+9pnarZQAqnRYEU4btNw7VKN2j/Xghci+6qHRafAXT/AH/c5hHtwF9SWB/BCCvYZbe+ZI8l2z583Q2xYUAmHpDB+ou8SUh3Cwl+25Tb6hLDjdLAbgCt2JPqfeFVTkAAAAASUVORK5CYII=">
                        </td>
                    </tr>
                </tbody>
            </table>
            <p class="goBack">back to search</p>
        </div>
    `;
}

// Handles go back to search
document.addEventListener('click', function (event) {
	if (!event.target.matches('.goBack')) return;
	event.preventDefault();
	showFarmSearch();
}, false);

// Initial setup
const observer = new MutationObserver((mutationsList) => {
    if (FARM_INFO_BLOCK_STATE === "SHOWN") return;
    for (const mutation of mutationsList) {
      if (mutation.type === 'childList' && mutation.addedNodes.length > 0) {
        const addedNode = mutation.addedNodes[0];
        if (addedNode.classList.contains('indiana-scroll-container--hide-scrollbars')
        && addedNode.classList.contains('indiana-scroll-container')
        && addedNode.classList.contains('page-scroll-container')) {
          FARM_INFO_BLOCK_STATE = "SHOWN";
          showFarmLoading();
          document.body.appendChild(farmInfoDiv);
          setTimeout(() => {
            showFarmSearch();
          }, 3000);
        }
      }
    }
  });

observer.observe(document.body, { childList: true, subtree: true });

// Styles
function addStyle(styleString) {
    const style = document.createElement('style');
    style.textContent = styleString;
    document.head.append(style);
}

addStyle(`
    .farmInfo {
        display: flex;
        justify-content: center;
        position: absolute;
        left: calc(50% - 200px);
        bottom: 7.875px;
        margin-left: auto;
        margin-right: auto;
        width: 400px;
        z-index: 1055;

        font-size: 14px;
        border-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAkAAAAJAgMAAACd/+6DAAAABGdBTUEAALGPC/xhBQAAAAlQTFRFAAAA6tSqGBQlHYAABgAAAAF0Uk5TAEDm2GYAAAAZSURBVAjXY+BawcCgGsbAMIGxAQODxIHyAIsgB7CF1qipAAAAAElFTkSuQmCC) 22.2222% / 1 / 0 repeat;
        border-style: solid;
        border-width: 5.25px;
        image-rendering: pixelated;
        border-radius: 13.125px;
        padding: 2.625px;
    }

    .farmInfoTable th, .farmInfoTable td {
        font-weight: 500;
        padding: 0.25rem 0.75rem;
        text-align: center;
    }

    .farmInfoTable p {
        display: inline;
    }

    .farmInfoIcon {
        display: inline;
        width: 18px;
        height: 18px;
        object-fit: contain;
        opacity: 1;
    }

    .farmInput {
        border: 1px solid white;
        border-radius: 5px;
        background-color: rgb(194 134 105 / var(--tw-bg-opacity));
        padding: 0.25rem 0.75rem;
        margin-right: 0.5rem;
    }

    .farmInput::placeholder {
        color: white;
        opacity: 1;
    }


    .farmInput:focus-visible {
        outline: none;
    }

    .farmInput::-internal-autofill-selected {
        background-color: rgb(194 134 105 / var(--tw-bg-opacity)) !important;
    }

    .goBack {
        font-size: 12px;
        margin-top: 0.5rem;
        text-decoration: underline;
        cursor: pointer;
        text-align: center;
    }

    .formInfoTitle {
        margin: 0.25rem 0 0.55rem 0;
        text-align: center;
        font-size: 18px;
    }
`);
